-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Апр 13 2018 г., 10:49
-- Версия сервера: 5.6.37
-- Версия PHP: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `graduate_work`
--

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id`, `title`) VALUES
(1, 'First Category'),
(2, 'Second Category'),
(3, 'Third Category');

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(65, '2014_10_12_000000_create_users_table', 1),
(66, '2014_10_12_100000_create_password_resets_table', 1),
(67, '2018_04_10_192049_create_categories_tables', 1),
(68, '2018_04_11_050716_create_questions_table', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `questions`
--

CREATE TABLE `questions` (
  `id` int(10) UNSIGNED NOT NULL,
  `question` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` text COLLATE utf8mb4_unicode_ci,
  `category_id` int(11) NOT NULL,
  `author` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_author` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `questions`
--

INSERT INTO `questions` (`id`, `question`, `answer`, `category_id`, `author`, `email_author`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Nulla voluptate ipsa officiis sunt. Autem eos vero vel ullam et ut ab reprehenderit. Sapiente velit qui non optio. Omnis perferendis quis repudiandae repellendus numquam aut.', 'Voluptates est et officia et quisquam ipsum sequi. Sed et velit quia ea. Quia reiciendis velit nihil ipsam numquam reprehenderit qui. Molestiae et voluptas dolorem autem aut dolor eligendi.', 1, 'Bernhard', 'fblanda@hotmail.com', 0, '2002-12-03 11:19:30', '1980-04-23 11:45:30'),
(2, 'Iure nam vel illum nostrum dolore. Dolorem dolore consequatur et aut. Possimus delectus quia quia corrupti aut molestiae quo. Neque totam rerum atque a. Veniam reprehenderit sit commodi corrupti qui.', 'Reiciendis quidem magnam sapiente. Qui voluptas itaque at et. Fugit quia debitis ut quia maiores sint.', 2, 'Wisoky', 'rodriguez.ashly@labadie.net', 0, '1995-01-24 02:01:55', '2007-09-01 19:16:03'),
(3, 'Omnis ipsa facere ipsam enim suscipit et. Et inventore vitae sed ex eos consequatur. Repellendus illo quo quibusdam non eligendi nemo.', 'Qui amet dignissimos qui cumque aut quibusdam expedita. Nam enim voluptatem blanditiis sed repellendus ut eligendi. Nisi assumenda iusto rem omnis.', 3, 'Simonis', 'camilla00@gmail.com', 1, '2013-09-10 13:41:56', '2014-06-19 02:07:35'),
(4, 'Sit alias aspernatur accusamus quia vel provident. Veritatis officiis molestiae unde. Doloremque consequatur iure est veritatis eaque exercitationem fugit.', 'Quam amet occaecati ea eaque neque. Quas voluptas labore voluptatem animi. Fuga voluptatum autem ut veniam dolorem.', 1, 'Kautzer', 'halle07@nader.com', 1, '1978-06-24 18:43:10', '1972-05-25 17:11:09'),
(5, 'Beatae explicabo exercitationem pariatur expedita aut eum. Sint ex exercitationem qui ducimus reprehenderit impedit autem. Enim tempora nihil molestiae sit excepturi. Ea nesciunt voluptas sit.', 'Sed eligendi et accusantium voluptatem praesentium autem. Qui et numquam sequi est. Est tempora iure sapiente optio voluptatem et sed. Ut repudiandae cum voluptas omnis debitis officia pariatur.', 1, 'Robel', 'sschamberger@hotmail.com', 0, '1972-02-04 23:41:55', '1996-08-12 02:54:36'),
(6, 'Possimus quia voluptatum quae in sit quae sit. Fugit sint officia amet officia tempore.', 'Nesciunt rem tempora voluptatum et quia pariatur et. Odio officia libero impedit accusamus aliquam. Quasi qui aliquam natus non repellendus. Est expedita dolor at pariatur tempore sunt.', 2, 'Hoppe', 'samir.schmitt@mraz.com', 0, '1994-10-18 13:41:45', '2014-07-06 23:42:24'),
(7, 'Est totam sed qui dolore ab aperiam numquam. Doloremque commodi hic quibusdam sit et. Ut dolores sed qui dicta consequuntur accusantium. Itaque non corporis totam at quia quo.', 'Libero rerum optio reprehenderit expedita neque quasi. Ut quibusdam aut provident eveniet. Eligendi fugit itaque eligendi dolorem consequuntur.', 2, 'Cartwright', 'syost@gmail.com', 0, '1989-08-12 15:44:58', '1981-07-11 16:22:13'),
(8, 'Est aliquid modi natus sunt quisquam quas architecto. Ut qui reiciendis vero eum laborum.', 'Delectus quo quis sit illo porro harum quibusdam. Modi unde ea accusamus aut vel quasi. Consectetur sequi adipisci fuga quod nemo aut.', 2, 'Jast', 'gaylord.delfina@davis.biz', 0, '1986-04-25 11:24:44', '2010-06-13 06:07:35'),
(9, 'Quis non voluptatem qui. Excepturi quos sint hic repellendus sunt temporibus. Temporibus consequuntur dicta corrupti nemo molestiae. Voluptatum debitis voluptatum voluptas et quia.', 'Natus iste et commodi perspiciatis. Placeat tempore dolorem porro nostrum asperiores. Sunt rem ipsum beatae. Quis adipisci quo fuga omnis a.', 2, 'Batz', 'gerardo37@jerde.info', 0, '1976-04-27 16:29:25', '1973-05-13 21:31:45'),
(10, 'Cum mollitia quis sit. Harum eum explicabo quo vitae at impedit. Quaerat necessitatibus quis voluptatibus eos quam rerum et deserunt.', 'Dolor occaecati sequi omnis repudiandae eos commodi. Vero enim ullam consequatur reprehenderit. Similique laboriosam suscipit officia incidunt esse blanditiis. Accusamus dolores neque ex et dolorum.', 2, 'Schowalter', 'camila40@kozey.info', 1, '1976-04-02 01:08:49', '1985-05-20 03:04:54'),
(11, 'Sit doloribus cupiditate fuga aut deleniti et distinctio. Ullam id odit et vero. Minus illo atque a. Cupiditate ipsum ut molestiae molestias.', 'Nam sit ut ipsa et tempore consequatur. Aut natus aut temporibus magni laboriosam. Consectetur quia minima est et aspernatur molestiae eaque.', 3, 'Terry', 'warren.lebsack@leannon.com', 1, '2003-11-05 10:41:48', '1980-12-17 07:29:49'),
(12, 'Sapiente voluptas quia aut architecto. Commodi est inventore qui delectus saepe enim doloremque. Incidunt molestiae itaque voluptatibus et necessitatibus quia.', 'Aut vel sint rerum qui et et error vel. Cumque aut ut rem et ratione. Quo officia ex ad quo ut.', 3, 'Moore', 'giles.mckenzie@gmail.com', 1, '1986-01-22 16:19:16', '1988-12-22 05:26:58'),
(13, 'Quibusdam eligendi accusamus officia at fuga ut. Ex deserunt numquam laborum praesentium eos. Et omnis fugiat et id. Cumque aliquid iste quibusdam sit asperiores.', 'Sit aut sapiente consequatur assumenda quis. Autem at officiis sequi iste. Provident cum nostrum labore aut blanditiis.', 2, 'Torphy', 'zack87@hilpert.info', 0, '1997-06-25 00:08:33', '1980-03-28 03:26:04'),
(14, 'Maiores error aut nobis praesentium adipisci. Sunt incidunt et neque unde fugit minus. Quia repudiandae non eveniet praesentium.', 'Unde in fugit et voluptatem. Sed eveniet eaque earum voluptatem asperiores. Iusto et itaque velit.', 3, 'Bartoletti', 'burdette.romaguera@runte.biz', 1, '1988-01-22 21:04:24', '2010-04-30 02:35:47'),
(15, 'Fuga laborum vitae dolore dolorem. Adipisci ratione aut non eius impedit illo. Aspernatur porro est id qui vero totam. Temporibus suscipit voluptatem saepe occaecati exercitationem.', 'Quae quas aliquid explicabo nulla sequi laborum. Nostrum asperiores nisi expedita et tempore. Mollitia ut iste sint consequatur.', 1, 'Hoeger', 'juanita.kutch@muller.biz', 1, '1972-08-13 14:04:33', '2017-04-23 07:34:52'),
(16, 'Labore ut consequatur aliquid sit et. Quis incidunt adipisci repudiandae qui.', 'Quo asperiores odit pariatur voluptatem nostrum. Deserunt ipsam aut iure quia. Asperiores facilis placeat provident est mollitia et doloremque sit.', 1, 'Langworth', 'cassandra27@yahoo.com', 1, '1996-05-07 06:04:07', '2018-03-21 16:29:26'),
(17, 'Qui esse voluptas voluptatum consectetur qui quo. Et ea eveniet quos vel eaque. Dolores quia et ut officia quia distinctio molestiae facere.', 'Aperiam ad reprehenderit quibusdam aspernatur. Distinctio ad cum exercitationem iure quia magni autem eos.', 3, 'Senger', 'tfeil@bogan.com', 1, '1981-03-10 20:36:59', '1995-02-16 16:07:08'),
(18, 'Recusandae dolore est molestiae quis enim dolor. Aut reprehenderit sit omnis fuga enim velit. Quia recusandae perspiciatis similique non veniam explicabo ipsa neque.', 'Voluptatem eaque soluta facilis totam voluptatem. Sed quasi fugit sint.', 3, 'Hudson', 'frederique.armstrong@hotmail.com', 1, '2015-01-12 13:39:38', '2007-01-17 11:20:19'),
(19, 'Voluptates sunt sequi quos. Ea quos dicta nemo magnam quae. Doloremque molestiae incidunt et assumenda inventore. Suscipit voluptatem aut blanditiis ducimus quia animi.', 'Saepe et et aperiam animi minima quam dolorum. Exercitationem sed incidunt vel esse. Saepe sapiente cumque itaque omnis aut facere debitis.', 2, 'Gislason', 'donald.padberg@yahoo.com', 0, '1980-06-02 22:35:28', '2007-11-17 11:55:34'),
(20, 'Ea recusandae optio eos temporibus exercitationem. Tempore fugiat minima repellendus est. Adipisci eos maxime et. Nemo est est aut dignissimos.', 'Velit voluptatem consequuntur dolore omnis consequatur neque. Rem velit vitae ut magnam voluptatem mollitia veritatis.', 1, 'Cruickshank', 'ohara.carolyne@upton.com', 0, '2010-08-13 23:08:56', '2017-04-20 09:16:07'),
(21, 'In perferendis provident rerum at. Atque aliquam ut id tempora voluptatem culpa et. Vero molestiae laudantium quis consequatur ad adipisci.', 'Minima ut qui ut tempora esse eum. Sed id dolores et aut ipsam optio neque. Eveniet rerum ullam doloribus fugiat culpa.', 1, 'Koepp', 'dbrakus@dubuque.com', 1, '1995-11-16 07:08:58', '2004-08-21 09:55:19'),
(22, 'Accusamus vitae voluptates incidunt nostrum cupiditate. Sed nisi et minus quam quisquam omnis voluptate. Minima repellat cupiditate qui nesciunt. Nihil velit dicta in est.', 'Distinctio doloribus ex aut quisquam omnis est aut. Natus labore sunt nobis repellat et recusandae harum.', 2, 'Schimmel', 'jayson06@mccullough.com', 0, '1994-05-28 12:41:37', '2013-05-30 05:04:48'),
(23, 'Unde ut nihil recusandae perspiciatis consequatur neque. Qui labore ut officiis nihil. Cum voluptas dolorem corporis quae optio natus quas qui. Dolore ipsa autem et maiores assumenda dolorem.', 'Id soluta asperiores in sint est dolorem incidunt et. Veritatis cumque vel rerum debitis impedit qui quaerat. Enim sint beatae enim at sit iusto. Est et fugit qui consequatur.', 1, 'Rolfson', 'wayne30@yahoo.com', 0, '1971-10-04 07:40:09', '2015-07-17 18:56:24'),
(24, 'Officiis asperiores sint harum laborum sed voluptas totam. Quo magni maxime dolores eaque aut dicta. Cum error sit unde. Nulla excepturi odio placeat a.', 'Ullam sit natus ut culpa dignissimos amet. Dolorum numquam culpa sint nisi itaque magni et. A dolorem blanditiis aut hic distinctio.', 3, 'Lowe', 'amy41@cummings.biz', 0, '1971-03-02 04:30:55', '1986-10-09 06:04:49'),
(25, 'Veritatis ut dolorem quos nemo et quaerat aut. At et sint laudantium cupiditate rem enim neque sunt. Quod consequuntur et laudantium autem nam omnis.', 'At et et corrupti aut dolores aut consequatur. Voluptate eius id aperiam non voluptas. Assumenda temporibus magni alias dolorem.', 1, 'Bins', 'charlotte72@collins.com', 1, '1981-07-05 20:27:46', '1993-04-22 20:25:22');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@admin.ru', '$2y$10$VyTBlJ00E6Kd86lvEWqYA.4LQbSn9o0ryMs3rOKwoEzmmptrzc1AO', NULL, '2018-04-13 04:26:01', '2018-04-13 04:26:01');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT для таблицы `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
