# Vadik graduate work
---
The project is created on the basis of the task to the diploma work at the course of php. It allows users to view questions and answers in different categories, and also allows you to ask new questions. In the project there has an admin panel. With its help, you can create, edit and delete categories, questions, answers with publication on the site and without publishing. All content is listed [here](https://netology-university.bitbucket.io/php/graduate-work/faq/index.html)(no additional task was performed).

---

##Requirements
* PHP 7.1.3 or higher;
* PDO-SQLite PHP extension enabled;
---

##Install
    git clone https://parapupkin@bitbucket.org/parapupkin/graduate-work.git
    cd graduate-work
    composer install
    php artisan migrate 

To generate random data, you can use 
 
    php artisan db:seed

---

##Admin mode

To enter admin panel, add to the main page "/admin" (for example, graduate-work/admin). By default, one administrator is created with the name admin and the password admin. (the administrator is created only after executing the command in the console 'php artisan db: seed')