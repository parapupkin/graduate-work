@extends('layouts.app_form')
@section('title', 'Create question')
@section('content')
    <h1>Введите свои данные и текст вопроса</h1>
    <form method="post" action="/question/store">
        {{ csrf_field() }}
    	<input type="text" name="author" placeholder="Имя" required="required" />
        <input type="email" name="email_author" placeholder="Email" required="required" />
        <select name="category_id">
            @foreach ($categories as $category)
            <option value="{{ $category->id }}">{{ $category->title }}</option>
            @endforeach
        </select>
        <textarea name="question" placeholder="Введите сюда текст вопроса" required="required"></textarea>
        <button type="submit" class="btn btn-primary btn-block btn-large">Задать вопрос</button>
    </form>
@endsection