@extends('layouts.app')

@section('content')
<h2 class="admin">Здравствуйте,<br> уважаемый {{ Auth::user()->name }}! <br> Выберите из списка, <br> что Вы хотите сделать </h2>
<a class="btn btn-primary btn-block btn-large">Работать с адиминистраторами</a>
<a href="/admin/action/category" class="btn btn-primary btn-block btn-large">Работать с категориями</a>
<a class="btn btn-primary btn-block btn-large">Работать с вопросами и ответами</a>
<a class="admin" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
    Выйти
</a>
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
</form>
@endsection
