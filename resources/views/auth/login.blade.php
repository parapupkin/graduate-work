@extends('layouts.app')
@section('title', 'Login')
@section('content')
    <h1>Уважаемый, <br> хотите админить - авторизуйтесь :)</h1>
    <form method="POST" action="{{ route('login') }}">
        @csrf
        @if ($errors->has('name') || $errors->has('password'))            
            <span class="errors">Неверный логин или пароль</span>                        
        @endif   
        <input id="name" type="name" placeholder="login" name="name" required autofocus>          
        <input id="password" type="password" placeholder="password" name="password" required>        
        <button type="submit" class="btn btn-primary btn-block btn-large">Авторизоваться</button>
    </form>     
@endsection



                           
                       
                                

                                
                            
