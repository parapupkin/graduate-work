<!doctype html>
<html lang="ru" class="no-js">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/reset.css"> <!-- CSS reset -->
    <link rel="stylesheet" href="css/style.css"> <!-- Resource style -->
    <script src="js/modernizr.js"></script> <!-- Modernizr -->
    <title>FAQ</title>
</head>
<body>
<header>
    <h1>Вопросы и ответы или... диплом Устиновича Вадима :)</h1>        
</header>
<section class="cd-faq">
    <ul class="cd-faq-categories">        
        @foreach ($categories as $category)
        <li><a href="#{{$category->title}}">{{ $category->title }}</a></li>
        @endforeach         
    </ul> <!-- cd-faq-categories -->
    @foreach ($categories as $category)
    <div class="cd-faq-items">
        <ul id="{{$category->title}}" class="cd-faq-group">
            <li class="cd-faq-title"><h2>{{ $category->title }}</h2></li>
            @foreach ($questions as $question)            
            @if ($question->category->title == $category->title and @!empty($question->answer) and $question->status != 0 )                
            <li>                
                <a class="cd-faq-trigger" href="#0">{{ $question->question }}</a>                
                <div class="cd-faq-content">
                    <p>{{ $question->answer }}</p>
                </div> <!-- cd-faq-content -->
            </li>            
            @endif 
            @endforeach           
        </ul> <!-- cd-faq-group -->        
    </div> <!-- cd-faq-items -->
    @endforeach
    <a href="#0" class="cd-close-panel">Close</a>    
</section> <!-- cd-faq -->
<footer>
    <div class="vertical">
        <p class="text-center vertical">Остались вопросы - перейдите по ссылке</p> 
        <a class="vertical" href="/ask"><img src="{{ asset('img/vopros_button-min.png') }}" alt="Задать вопрос"></a>        
    </div>    
</footer> 
<script src="js/jquery-2.1.1.js"></script>
<script src="js/jquery.mobile.custom.min.js"></script>
<script src="js/main.js"></script> <!-- Resource jQuery -->
</body>
</html>